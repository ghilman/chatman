import React, { Component } from 'react';
import imgLogin from '../assets/login.png';
import './style.css';

class UsernameForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  onSubmit(e) {
    e.preventDefault()
    this.props.onSubmit(this.state.username)
  }

  onChange(e) {
    this.setState({ username: e.target.value })
  }

  render() {
    return (
      <div>
        

        <div className="login-ts">
            <div className="row bs-reset">
                <div className="col-md-5 login-container bs-reset">
                    <div className="login-content">
                        <h1>Selamat Datang di <br /> ChatMan</h1>
                        <p>Aplikasi Chat yang menggunakan platform ChatKit</p>
                        <form onSubmit={this.onSubmit} className="login-form">
                            <div className="form-group">
                                <label>Masukkan Nama Kamu</label>
                                <input className="form-control" type="text" placeholder="Contoh: Joni" onChange={this.onChange} />
                            </div>
                            <input className="btn btn-lg btn-supernova" type="submit" value="Masuk" />
                        </form>
                    </div>
                </div>
                <div className="col-md-7 bs-reset bg-grd">
                    <div className="login-bg">
                      <img src={imgLogin} className="login-logo" alt="Ghilman Prayogi" />
                    </div>
                </div>
            </div>
        </div>
      </div>
    )
  }
}

export default UsernameForm
