import React, { Component } from 'react'

class WhosOnlineList extends Component {
  renderUsers() {
    return (
      <ul>
        {this.props.users.map((user, index) => {
          if (user.id === this.props.currentUser.id) {
            return (
              <WhosOnlineListItem key={index} presenceState="online">
                {user.name} (You)
              </WhosOnlineListItem>
            )
          }
          return (
            <WhosOnlineListItem key={index} presenceState={user.presence.state}>
              {user.name}
            </WhosOnlineListItem>
          )
        })}
      </ul>
    )
  }

  render() {
    const styles = {
      loading: {
        fontFamily: 'Source Sans Pro',
        fontSize: 20,
        marginTop: 20,
        marginLeft: 20,
      }
    }
    if (this.props.users) {
      return this.renderUsers()
    } else {
      return <p style={styles.loading}>Loading...</p>
    }
  }
}

class WhosOnlineListItem extends Component {
  render() {
    const styles = {
      li: {
        display: 'flex',
        fontSize: 16,
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#fff',
        borderBottom: '1px solid #e9ebeb'
      },
      div: {
        borderRadius: '50%',
        width: 11,
        height: 11,
        marginRight: 10,
      },
      
    }
    return (
      <li style={styles.li} className="whos-bubble">
        <div
          style={{
            ...styles.div,
            backgroundColor:
              this.props.presenceState === 'online' ? '#539eff' : 'rgb(227, 227, 227)',
          }}
        />
        {this.props.children}
      </li>
    )
  }
}

export default WhosOnlineList