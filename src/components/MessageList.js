import React, { Component } from 'react'

class MessagesList extends Component {
  render() {
    const styles = {
      container: {
        overflowY: 'auto',
        flex: 1,
        
      },
      ul: {
        listStyle: 'none',
        paddingRight: 10,
        maxHeight: 'calc(87vh - 115px)',
        overflow: 'auto'
      },
      li: {
        marginTop: 13,
        marginBottom: 13,
        marginright: 13,
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 10,
        paddingBottom: 5,
        backgroundColor: 'rgb(246, 246, 246)',
        borderRadius: 10
      },
      senderUsername: {
        fontWeight: 'bold',
      },
      message: { fontSize: 15 },
    }
    return (
      <div
        style={{
          ...this.props.style,
          ...styles.container,
        }}
      >
        <ul style={styles.ul}>
          {this.props.messages.map((message, index) => (
            <li key={index} style={styles.li}>
              <div>
                <span style={styles.senderUsername}>{message.senderId}</span>{' '}
              </div>
              <p style={styles.message}>{message.text}</p>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

export default MessagesList
